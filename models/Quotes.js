const mongoose = require('mongoose');

const quotesSchema = new mongoose.Schema({
	quote: { type: String, required: true },
	author: { type: String, required: true },
	category: { type: String, required: true },
	isActive: { type: Boolean, default: true },
	createdOn: { type: Date, default: new Date }
})

const Quotes = mongoose.model('Quote', quotesSchema);

module.exports = Quotes;