const mongoose = require('mongoose');

const usersSchema = new mongoose.Schema({
	email: { type: String, required: true },
	password: { type: String, required: true },
	isAdmin: { type: Boolean, default: false },
	createdOn: { type: Date, default: new Date }
})


const Users = mongoose.model('User', usersSchema);

module.exports = Users;