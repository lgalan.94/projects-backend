const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routers/userRoutes');
const quoteRoutes = require('./routers/quoteRoutes');

const app = express();

const port = 3001;

// db connection

mongoose.connect("mongodb+srv://admin:admin@batch288galan.q03dxqw.mongodb.net/Quote-Generator-API?retryWrites=true&w=majority", {useNewUrlParser: true}, {useUnifiedTopology: true});

const db = mongoose.connection;
db.once("error", console.error.bind(console, "Error connecting to database!"));
db.on("open", () => console.log("Successfully connected to cloud database!"));

//middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended:true }));
app.use('/user', userRoutes);
app.use('/quotes', quoteRoutes)

app.listen(port, () => console.log(`Server is running on port ${port}`));