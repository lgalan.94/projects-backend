const Quotes = require('../models/Quotes');
const auth = require('../auth.js');

module.exports.addQuotes = (request, response) => {

		const userData = auth.decode(request.headers.authorization);
		if (userData.isAdmin) {
			let quotes = request.body.map(quote => {
				return new Quotes({
					quote: quote.quote,
					author: quote.author,
					category: quote.category
				});
			});

			Quotes.insertMany(quotes)
			.then(saved => response.send(true))
			.catch(error => response.send(false));
		} else {
			return response.send(false);
		}
}


module.exports.retrieveAllQuotes = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if (userData.isAdmin) {
		Quotes.find({})
		.then(result => {
			if (result.length === 0) {
				return response.send(false)
			} else {
				return response.send(result);
			}
		})
		.catch(error => response.send(false))
	} else {
		return response.send(false)
	}
}


module.exports.getActiveQuotes = (request, response) => {
	Quotes.find({ isActive: true })
	.then(result => {
		if (result.length === 0) {
			return response.send(`No active quotes`)
		} else {
			return response.send(result)
		}
	})
	.catch(error => response.send(error));
}

