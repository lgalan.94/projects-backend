const Users = require('../models/Users');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

module.exports.registerUser = (request, response) => {

		Users.findOne({ email: request.body.email })
		.then(result => {
			if (!result) {
				let newUser = new Users({
					email: request.body.email,
					password : bcrypt.hashSync(request.body.password, 10)
				})

				newUser.save()
				.then(saved => response.send(true))
				.catch(error => response.send(error))
			} else {
				response.send(false)
			}
		})
		.catch(error => response.send(false))

}


module.exports.authentication = (request, response) => {

		Users.findOne({ email: request.body.email })
		.then(result => {
			
				if (result) {
					const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

					if (isPasswordCorrect) {
						return response.send({ auth: auth.createAccessToken(result) })
					} else {
						return response.send(false)
					}
				} else {
					return response.send(false)
				}

		})
		.catch(error => response.send(false));
}


module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	Users.findOne({ _id: userData.id })
	.then(result => response.send(result))
	.catch(error=> response.send(false));
}