const quotesControllers = require('../controllers/quotesControllers');
const auth = require('../auth.js')
const express = require('express');

const router = express.Router();

router.post('/add-quotes', auth.verify, quotesControllers.addQuotes)
router.get('/all-quotes', auth.verify, quotesControllers.retrieveAllQuotes);
router.get('/active-quotes', quotesControllers.getActiveQuotes);

module.exports = router;