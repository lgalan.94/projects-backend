const express = require('express');
const userControllers = require('../controllers/userControllers');
const auth= require('../auth.js');

const router = express.Router();

router.post('/register', userControllers.registerUser)
router.post('/login', userControllers.authentication);
router.get('/user-details', auth.verify, userControllers.retrieveUserDetails)

module.exports = router;